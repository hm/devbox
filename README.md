# Virtuelle Maschine für Fakultät 7 Praktika

In diesem Repository finden sich die Quellen für die virtuelle Maschine `devbox` für den Einsatz in den praktischen Übungen an der Fakultät 7.

Die Maschine wird für Virtual Box automatisch erstellt und eingerichtet. Es findet sich außerdem ein Skript im Home-Ordner des Benutzers, um auch während des Semesters noch Änderungen an der Maschine vorzunehmen.

Im Normalfall muss die Maschine durch Dozierende nicht selbst gebaut und verteilt werden. Stattdessen finden sich regelmäßige Releases `hier <https://gitlab.lrz.de/hm/devbox/-/wikis/Releases>`_. Basierend auf diesen Releases können Updates für Veranstaltungen über `Ansible <https://www.ansible.com/>`_ durchgeführt. Dies geht völlig losgelöst von dem Basis-Image mit Ausnahme des `full`-Image, das auf den Rechnern der Fakultät 7 installiert wird.

Für die Studierenden `findet sich im Wiki Hilfestellung <https://gitlab.lrz.de/hm/devbox/-/wikis/home>`_.

## VM Benutzen

Der Benutzer ist:

- Benutzername: devbox
- Passwort: devbox

Mit diesen Credentials kann sich der Benutzer einloggen und hat auch volle sudo-Rechte. Das Passwort kann geändert werden.

## VM Updates

Die Updates werden im Ansible-Playbook eingepflegt und im Repository
eingecheckt. Sie können auch lokal getestet werden mit:

```sh
  vagrant provision
```

Innerhalb der VM müssen alle Benutzer das Update-Skript ausführen um die
Änderungen einzuspielen::

```sh
  ~/update.sh <kurs>
```

`<kurs>` ist dabei entweder ein Kurskürzel oder `all`.

## Eigene Module hinzufügen und administrieren

Ansible verwendet ein globales "Playbook" für die Updates in den Maschinen. Darin gibt es sogenannte "Rollen" (`role`) und die Veranstaltungen als `tags`. Dadurch können sich Veranstaltungen gemeinsame Konfugrationen, Programme und Dateien etc. teilen. Das Vorgehen für eine neue Veranstaltung ist also:

1. Einen sinnvollen Tag für die Veranstaltung überlegen (siehe Ende von https://gitlab.lrz.de/hm/devbox/-/blob/main/ansible/playbook.yml)
2. Den Tag zu Rollen hinzufügen, die notwendig sind
3. Weitere Rollen in `ansible/roles` hinzufügen (am besten inspirieren lassen von den vorhandenen Rollen)

Merge request für die Änderungen durchführen.

Support und Diskussion findet sich hier: https://chat.hm.edu/group/devbox

## VM selbst bauen

### Voraussetzungen

Es wird benötigt:

- `Virtual Box <https://www.virtualbox.org/>`_
- `Vagrant <https://www.vagrantup.com/>`_
- `Ansible <https://www.ansible.com/>`_

Unter Debian/Ubuntu/Linux Mint zum Beispiel

```sh
  sudo apt install -y virtualbox vagrant ansible
```

Unter MacOS mit `Homebrew <https://brew.sh/>`

```sh
  brew install virtualbox virtualbox-extension-pack vagrant ansible
```

### VM Erstellen

Checkout des Repositories::

  git clone https://gitlab.lrz.de/hm/devbox
  cd devbox
  git submodule update --recursive --init

Dann Vagrant ausführen. Die Maschine wird automatisch provisioniert
(eingerichtet). Am Ende wieder runterfahren.

```sh
  cd vagrant
  vagrant up
  vagrant halt
```

Die VM ist nun fertig zur Verteilung bzw. weiteren Einrichtung. Einfach ganz
normal über Virtual Box benutzen.

(Eigene/neue/geränderte) Tags lassen sich lokal testen mit

```sh
  export DEVBOX_TAGS=mytag; vagrant up
```

### Testen eines Branches

```sh
  BRANCH=<branch-name> ./update.sh <tag>
```


## Release bauen

1. VM bauen

   ```sh
   vagrant up
   ```

1. VM updaten

   ```sh
   vagrant ssh -c "sudo apt update"
   vagrant ssh -c "sudo apt dist-upgrade"
   vagrant ssh -c "sudo apt clean"
   ```

1. qemu image exportieren

   ```sh
   qemu-img convert -c box-disk001.vmdk devbox-minimal-<semester>.qcow2
   ```

1. VirtualBox starten und einmal `update.sh` ausführen (wegen guest additions, TODO: automatisieren)

1. VirtualBox runterfahren und exportieren

   ```sh
   VBoxManage export devbox --output devbox-wise24.ova
   ```