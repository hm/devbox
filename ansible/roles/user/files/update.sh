#!/bin/bash -e
export ANSIBLE_FORCE_COLOR=true
export PYTHONUNBUFFERED=1
tags=${1:-always}
ansible-pull -U ${URL:-https://gitlab.lrz.de/hm/devbox} ansible/playbook.yml -C ${BRANCH:-main} -t ${tags} -e ansible_python_interpreter=auto
 

